// +build !windows

package ffutil

import (
	"os"
	"path/filepath"
)

func HiddenFile(namePattern string) (*os.File, error) {
	dir := filepath.Dir(namePattern)
	base := filepath.Base(namePattern)
	name := filepath.Join(dir, "."+base)
	return os.Create(name)
}
