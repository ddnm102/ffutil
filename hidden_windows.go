package ffutil

import "syscall" // on windows this gets you the windows version

func HiddenFile(filePath string) error {
	nameptr, err := syscall.UTF16PtrFromString(filePath)
	if err != nil {
		return err
	}
	// Do whatever windows calls are needed to change
	// the file into a hidden file; something like
	err = syscall.SetFileAttributes(nameptr, syscall.FILE_ATTRIBUTE_HIDDEN|syscall.FILE_ATTRIBUTE_SYSTEM)
	if err != nil {
		return err
	}

	return nil
}
