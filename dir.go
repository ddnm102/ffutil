package ffutil

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
)

// this is the default sort order of golang ReadDir
func SortFileNameAscend(files []fs.DirEntry) {
	sort.Slice(files, func(i, j int) bool {
		return files[i].Name() < files[j].Name()
	})
}

func SortFileNameDescend(files []fs.DirEntry) {
	sort.Slice(files, func(i, j int) bool {
		return files[i].Name() > files[j].Name()
	})
}

func PrintFiles(files []fs.DirEntry) error {
	for _, file := range files {
		fileInfo, err := file.Info()
		if err != nil {
			return err
		}
		// t, err := atime.Stat(file.Name())
		fmt.Println(file.Name(), fileInfo.Size(), fileInfo.ModTime())
	}
	return nil
}

// Files return all files in directory...
func Files(dir string, recursive bool, ext string) ([]fs.DirEntry, error) {
	files, err := os.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	filesOut := []fs.DirEntry{}

	for _, f := range files {
		// fmt.Printf("working on: %v\n", f.Name())
		if f.IsDir() {
			if recursive {
				// recursive for sub directory here....
				list, err := Files(filepath.Join(dir, f.Name()), recursive, ext)
				if err != nil {
					return nil, err
				}
				filesOut = append(filesOut, list...)
			}
			continue
		}

		if ext != "" {
			if filepath.Ext(f.Name()) == ext {
				// processing for ext file
				filesOut = append(filesOut, f)
			}
			continue
		}

		// case not specify ext
		filesOut = append(filesOut, f)
	}

	return filesOut, nil
}
