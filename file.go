package ffutil

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

//IsExist File Folder Is Exist
func IsExist(path string) bool {
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return true //da co
	}
	return false //chua co
}

// ComputeSHA256F is aculator the SHA256 of input file
func ComputeSHA256F(inFile string) (string, error) {
	f, err := os.Open(inFile)
	if err != nil {
		return "", errors.New("error while create sha256, inFile: " + inFile)
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", errors.New("error while create sha256: Err: " + err.Error())
	}

	shaValue := fmt.Sprintf("%x", h.Sum(nil))
	return shaValue, nil
}

// ComputeSHA256S is aculator the SHA256 of input string
func ComputeSHA256S(s string) (string, error) {
	newReader := strings.NewReader(s)

	h := sha256.New()
	if _, err := io.Copy(h, newReader); err != nil {
		return "", errors.New("error while create sha256 of: \"" + s + "\"")
	}

	shaValue := fmt.Sprintf("%x", h.Sum(nil))
	return shaValue, nil
}

func FileSize(p string) (int64, error) {
	f, err := os.Open(p)
	if err != nil {
		return 0, err
	}
	fi, err := f.Stat()
	if err != nil {
		return 0, err
	}
	return fi.Size(), nil
}

func CheckDifference(p1, p2 string, allowSize int64, unit string) bool {
	var allow int64 //gia tri khac nhau cho phep giua 2 files
	switch unit {
	case "B": //byte
		allow = allowSize
	case "K": //kilobyte
		allow = allowSize * 1024 // 1kb = 1024byte
	case "M": //megabyte
		allow = allowSize * 1024 * 1024 // 1mb = 1024kb
	case "G": //gigabyte
		allow = allowSize * 1024 * 2014 * 1024 // 1gb = 1024mb
	default:
		allow = allowSize //byte
	}
	s1, err := FileSize(p1)
	if err != nil {
		return false
	}
	s2, err := FileSize(p2)
	if err != nil {
		return false
	}

	gap := float64(s1 - s2)

	if math.Abs(gap) > float64(allow) {
		return false
	} else {
		return true
	}
}

// Dir copies a whole directory recursively
func CopyDir(src string, dst string) error {
	var err error
	var fds []os.FileInfo
	var srcinfo os.FileInfo

	if srcinfo, err = os.Stat(src); err != nil {
		return err
	}

	if err = os.MkdirAll(dst, srcinfo.Mode()); err != nil {
		return err
	}

	if fds, err = ioutil.ReadDir(src); err != nil {
		return err
	}
	for _, fd := range fds {
		srcfp := path.Join(src, fd.Name())
		dstfp := path.Join(dst, fd.Name())

		if fd.IsDir() {
			if err = CopyDir(srcfp, dstfp); err != nil {
				fmt.Println(err)
			}
		} else {
			if err = CopyFile(srcfp, dstfp, true); err != nil {
				fmt.Println(err)
			}
		}
	}
	os.RemoveAll(src)
	return nil
}

// File copies a single file from src to dst
func CopyFile(src, dst string, override bool) error {
	if !override && IsExist(dst) {
		return errors.New("destination file was existed.")
	}
	var err error
	var srcfd *os.File
	var dstfd *os.File
	var srcinfo os.FileInfo

	if srcfd, err = os.Open(src); err != nil {
		return err
	}
	defer srcfd.Close()

	if dstfd, err = os.Create(dst); err != nil {
		return err
	}
	defer dstfd.Close()

	if _, err = io.Copy(dstfd, srcfd); err != nil {
		return err
	}
	if srcinfo, err = os.Stat(src); err != nil {
		return err
	}
	return os.Chmod(dst, srcinfo.Mode())
}

func selfDelete(numberString string) {
	//tu xoa ban than minh
	//xoa file truoc
	tempDir := os.TempDir()
	filePath := filepath.Join(tempDir, "scratch.bat")
	os.Remove(filePath)
	createFile, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		fmt.Printf("err: %v\n", err)
	}
	//defer createFile.Close()
	contentString := "ping -n " + numberString + " 127.0.0.1  nul\r\n"
	contentString += "loop\r\n"
	contentString += "del \"" + os.Args[0] + "\"\r\n"
	contentString += "if exist \"" + os.Args[0] + "\" goto loop\r\n"
	contentString += "del %0"
	createFile.WriteString(contentString)
	createFile.Close()
	//
	cmd := exec.Command(filePath)
	cmd.Start()
}
